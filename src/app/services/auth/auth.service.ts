import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { generateUsuario, Usuario } from 'src/app/models/usuario.model';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _http: HttpClient,
    private _router: Router
  ) { }

  login(email: string, password: string): Observable<Usuario> {
    let user = generateUsuario();
    user.ID_ROL = 1;
    localStorage.setItem('user', JSON.stringify(user));
    return of(user)

    return this._http.post<Usuario>(environment.url_api + 'auth/login', {
      email,
      password,
    }).pipe((usuario) => {
      localStorage.setItem('user', JSON.stringify(usuario));
      return usuario
    })
  }

  SignOut() {
    localStorage.removeItem('user');
    this._router.navigate(['/authentication/login']);
  }

}

