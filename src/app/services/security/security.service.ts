import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(private _http: HttpClient,) { }

  get isLoggedIn(): boolean {
    //@ts-ignore
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }
  get dataUser(): Usuario {
    //@ts-ignore
    const user = JSON.parse(localStorage.getItem('user'));
    return user
  }
}
