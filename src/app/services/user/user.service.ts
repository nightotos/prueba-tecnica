import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Usuario, UsuarioForm } from 'src/app/models/usuario.model';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private _http: HttpClient,
  ) { }

  saveUser(userForm: UsuarioForm): Observable<Usuario> {
    return this._http.post<Usuario>(environment.url_api + 'users', {
      ...userForm
    })
  }

  editUser(userForm: UsuarioForm, id: number | string): Observable<Usuario> {
    return this._http.put<Usuario>(environment.url_api + 'users/' + id, {
      ...userForm
    })
  }

  delUser(id: number | string): Observable<boolean> {
    return this._http.delete<boolean>(environment.url_api + 'users/' + id)
  }

  getUser(id: number | string): Observable<Usuario[]> {
    return this._http.get<Usuario[]>(environment.url_api + 'users/' + id)
  }

  getAllUser(): Observable<Usuario[]> {
    return this._http.get<Usuario[]>(environment.url_api + 'users')
  }

}

