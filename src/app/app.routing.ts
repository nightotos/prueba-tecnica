import { Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { BlankComponent } from "./layouts/blank/blank.component";
import { FullComponent } from "./layouts/full/full.component";

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    data: {
      title: "Prueba Técnica",
    },
    children: [
      //lazy load usuario module
      {
        path: 'usuarios',
        loadChildren: () => import('./pages/usuario/usuario.module').then((m) => m.UsuarioModule)
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: "",
    component: BlankComponent,
    children: [
      {
        path: "authentication",
        loadChildren: () =>
          import("./authentication/authentication.module").then(
            (m) => m.AuthenticationModule
          ),
      },
    ],
  },
  {
    path: "**",
    redirectTo: "",
  },
]