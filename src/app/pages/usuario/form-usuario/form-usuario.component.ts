import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario, UsuarioForm } from 'src/app/models/usuario.model';
import { UserService } from 'src/app/services/user/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-usuario',
  templateUrl: './form-usuario.component.html',
  styleUrls: ['./form-usuario.component.css']
})
export class FormUsuarioComponent implements OnInit {
  title: string = ''
  userForm: FormGroup | null = null
  isEdit: boolean = false
  isLoading: boolean = false
  isSaving: boolean = false
  id: string | null = ''
  dataUsuarioEdit: Usuario | null = null
  constructor(private _formBuilder: FormBuilder, private _activateRoute: ActivatedRoute, private _userService: UserService, private _router: Router) { }

  ngOnInit(): void {
    this.formGenerate()


    if (this._activateRoute.snapshot.paramMap.has("id")) {
      this.isEdit = true;
      this.isLoading = true;
      this.title = "Editar";
      this.id = this._activateRoute.snapshot.paramMap.get("id");
      //@ts-ignore
      this._userService.getUser(this.id).subscribe(
        (usuario) => {

          this.dataUsuarioEdit = usuario[0];
          this.formGenerate();
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
          Swal.fire(
            "Error al buscar el usuario",
            "usuario no encontrado",
            "error"
          ).then(() => {
            this._router.navigate(["/usuarios/lista"]);
          });
        }
      );
    } else {
      this.title = "Nuevo";
      this.formGenerate();
    }

  }

  formGenerate(): void {
    this.userForm = this._formBuilder.group(
      {
        NOMBRE: [this.isEdit ? this.dataUsuarioEdit?.NOMBRE : '',
        Validators.required
        ],
        ID_ROL: [this.isEdit ? this.dataUsuarioEdit?.ID_ROL.toString() : '', Validators.required
        ],
        ACTIVO: [this.isEdit ? this.dataUsuarioEdit?.ACTIVO : '', Validators.required
        ]
      }
    )
  }

  Submit(): void {

    if (!this.userForm?.valid) {
      return
    }
    let userForm = this.userForm.value as UsuarioForm
    this.isSaving = true
    if (this.isEdit) {
      //@ts-ignore
      this._userService.editUser(userForm, this.id).subscribe((usuario) => {
        this.isSaving = false
        Swal.fire('usuario editado exitosamente', userForm.NOMBRE, 'success').then(() => {
          this._router.navigate(['/usuarios/lista']);
        })
      }, (error) => {
        this.isSaving = false
        Swal.fire('Error al editar el usuario', error, 'error')

      })


    } else {
      this._userService.saveUser(userForm).subscribe((usuario) => {
        this.isSaving = false
        Swal.fire('usuario guardado exitosamente', userForm.NOMBRE, 'success').then(() => {
          this._router.navigate(['/usuarios/lista']);
        })
      }, (error) => {
        this.isSaving = false
        Swal.fire('Error al guardar el usuario', error, 'error')

      })

    }

  }




}
