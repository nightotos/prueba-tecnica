import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Usuario } from 'src/app/models/usuario.model';
import { SecurityService } from 'src/app/services/security/security.service';
import { UserService } from 'src/app/services/user/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {
  isLoading: boolean = false;
  displayedColumns: string[] = [
    "id",
    "nombre",
    "rol",
    'activo',
    "opcion",
  ];

  dataSource: MatTableDataSource<Usuario> | null = null;
  dataUsuarios: Usuario[] = [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator | null = null
  @ViewChild(MatSort, { static: true }) sort: MatSort | null = null

  fechaMax: Date = new Date();
  inputFecha: FormControl = new FormControl(new Date());

  errorMensaje: string | null = null;

  userData: Usuario | null = null
  constructor(private userService: UserService, private _securityService: SecurityService) { }

  ngOnInit(): void {
    this.userData = this._securityService.dataUser
    this.buscarUsuarios()
  }

  buscarUsuarios() {
    this.userService.getAllUser().subscribe(usuarios => {
      this.dataUsuarios = usuarios
      this.asignarData()
    }, (error) => {
      this.isLoading = false
      Swal.fire('Error al cargar usuarios', 'solicitaremos la información nuevamente en instantes', 'error').then(() => {
        setTimeout(() => {
          this.buscarUsuarios()
        }, 2000)
      })
    })
  }

  asignarData() {
    this.dataSource = new MatTableDataSource(this.dataUsuarios);
    this.pagination()
  }

  pagination() {
    //@ts-ignore
    this.dataSource.paginator = this.paginator;
    //@ts-ignore
    this.dataSource.sort = this.sort;
  }

  Filter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    //@ts-ignore
    this.dataSource.filter = filterValue;
  }
  eliminarUsuario(usuario: Usuario) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: "No podrás revertir este proceso!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'sí!',
      cancelButtonText: 'cancelar!',
    }).then((result) => {
      if (result.isConfirmed) {

        this.userService.delUser(usuario.ID_USUARIO).subscribe((del) => {
          this.dataUsuarios = this.dataUsuarios.filter((infoUser) => infoUser.ID_USUARIO !== usuario.ID_USUARIO)
          this.asignarData()
          Swal.fire(
            'Borrado!',
            usuario.NOMBRE + ' ha sido borrado correctamente.',
            'success'
          )
        }, () => {
          Swal.fire('Error al guardar el usuario', 'Por favor intente nuevamente', 'error')
        })



      }
    })
  }
}
