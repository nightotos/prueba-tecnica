import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UsuarioRoutes } from './usuario.routing';
import { MaterialModule } from 'src/app/materials-module';
import { FormUsuarioComponent } from './form-usuario/form-usuario.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
@NgModule({
  declarations: [ListaUsuariosComponent, FormUsuarioComponent],
  imports: [
    CommonModule, MaterialModule, FlexLayoutModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(UsuarioRoutes)
  ]
})
export class UsuarioModule { }
