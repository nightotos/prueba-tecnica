import { Routes } from "@angular/router";
import { FormUsuarioComponent } from "./form-usuario/form-usuario.component";
import { ListaUsuariosComponent } from "./lista-usuarios/lista-usuarios.component";

export const UsuarioRoutes: Routes = [
  //lista de usuarios
  {
    path: 'lista',
    component: ListaUsuariosComponent,
    data: {
      title: 'Usuarios del Sistema'
    }
  },
  //crear usuario
  {
    path: 'nuevo',
    component: FormUsuarioComponent,
    data: {
      title: 'Crear usuario'
    }

  },
  //editar usuario
  {
    path: 'lista/editar/:id',
    component: FormUsuarioComponent,
    data: {
      title: 'Editar Usuario'
    }
  }
]