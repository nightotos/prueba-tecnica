import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userForm: FormGroup | null = null
  loading: boolean = false;
  errorMsg: string = '';

  constructor(private _formBuilder: FormBuilder, private _authService: AuthService, private _router: Router) { }

  ngOnInit(): void {
    this.formGenerate()
  }

  formGenerate(): void {
    this.userForm = this._formBuilder.group(
      {
        email: ['',
          Validators.compose([Validators.required, Validators.email])
        ],
        password: ['', Validators.compose([Validators.minLength(6), Validators.required])
        ]
      }
    )
  }

  Submit(): void {
    this.errorMsg = ''
    if (!this.userForm?.valid) {
      return
    }
    let userForm = this.userForm.value as UserForm
    this.loading = true
    //emular login
    setTimeout(() => {
      this._authService.login(userForm.email, userForm.password).subscribe(() => {
        this.loading = false
        this._router.navigate([""])
      }, () => {
        this.errorMsg = 'Error al iniciar sesión, por favor revisar la conexión'
        this.loading = false
      })
    }, 1000)

  }
}

interface UserForm { email: string; password: string }
