import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullComponent } from './layouts/full/full.component';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';
import { MaterialModule } from './materials-module';
import { BlankComponent } from './layouts/blank/blank.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    BlankComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, RouterModule.forRoot(AppRoutes, { enableTracing: false }), MaterialModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
