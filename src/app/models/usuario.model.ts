import * as faker from "faker";

export interface Usuario {
  ID_USUARIO: number;
  NOMBRE: string
  ID_ROL: 1 | 2 | 3,
  ACTIVO: 1 | 0
}

export interface UsuarioForm {
  NOMBRE: string
  ID_ROL: 1 | 2 | 3,
  ACTIVO: 1 | 0
}


export const generateUsuario =
  (): Usuario => {
    return {
      ID_USUARIO: faker.datatype.number(50),
      NOMBRE: faker.name.firstName() + ' ' + faker.name.lastName(),
      ID_ROL: faker.random.arrayElement([1, 2, 3]),
      ACTIVO: faker.random.arrayElement([1, 0]),
    };
  };

export const generateUsuarios = (
  count = faker.datatype.number({ min: 10, max: 20 })
): Usuario[] => {
  return Array.apply(null, Array(count)).map(() =>
    generateUsuario()
  );
};
